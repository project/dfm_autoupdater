----------------------------
About DFM AutoUpdater
----------------------------

DFM AutoUpdater allows a developer to enable Drupal to run autoupdates on 
a single domain (typically the dev domain for a site) for both 
Core and contrib modules on the first cron run every Thursday morning (GMT) 
(after 1700 Wed. Eastern, the latest that Drupal Security Advisories are supposed
to be released each week).

While autoupdates are its intended use, at its core, it will run any shell
script you want, really.  It is meant to automate the actions you'd normally
run manually while logged in via SSH to update a site.  As such it works by 
the module's cron hook opening a login shell connection back to the localhost
to carry out a series of commands on your behalf--essentially an SSH update proxy.

While similar results can be achieved by simply having cron run a shell script,
often the environment needs to be set up correctly and/or specific environmental
variables need to be setup in addition to site specific paths, permissions, etc.
I wanted something that would allow me to have a standardized method of updating
all my sites without getting too deep into the weeds with each implementation.
This module is meant to simplify and standardize the process.  Basically, 
if Drush or your tool of choice works whenever you're logged in via SSH, those 
commands will "just work" when the update script is run by the module.

There are many possibilities available, too.  If one uses Drush aliases and has
a master aliases list of multiple dev sites across multiple domains, it might be
possible to create a script to have a single master site autoupdate all aliased
sites.  There are probably many other possibilities I've not thought about yet.


----------------------------
Requirements
----------------------------

This module requires:
1.  SSH access to the host where autoupdates will run and SSH keys for that host's 
    SSH user.


----------------------------
Installation & Configuration
----------------------------

1.  Download, install, and enable module as normal.
2.  Copy update.sh from the dfm_autoupdater module directory and place it
    somewhere safe.
3.  Make sure that permissions on update.sh allow the SSH user to execute and run this file but
    that the file itself is either not accessible or without permission to be executed
    by the webserver user.  Most likely putting it outside webroot or in Drupal's 
    private files directory is sufficient if your .htaccess rules are as expected by Drupal.
4.  On the host you wish to autoupdate, append the public key for the host's SSH user
    to ~/.ssh/authorized_keys on the same host so that interactive password responses
    are not required for autoupdater to log into the localhost to do the updates from 
    the Drupal installation.  Suggested method:
    a)  Log into host via SSH
    b)  Execute following command (swapping out "id_rsa.pub" for the public key available, if needed):  
        cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
5.  On the host you wish to autoupdate, add the following line to the end of settings.php:
    $conf['autoupdate_environment'] = TRUE;
6.  Go to /admin/config/development/dfm-autoupdater and enter in:
    a)  AutoUpdate SSH Username -- the host's SSH username.
    b)  AutoUpdate Script Path -- the location where you copied the update.sh file
7.  Next modify the update.sh file to include whatever commands you will require to perform
    your updates and/or maintenance.  NOTE: If using Drush, make sure your commands use the '-y' 
    option to assume 'yes' as answer to all prompts. This prevents the script from hanging since 
    you can't answer any prompted input.
8.  To test, you may go to /admin/config/development/dfm-autoupdater and check the box 
    entitled "Run updates when cron is invoked manually", which will run your script whenever
    cron is manually invoked via the links on the Status Report page (/admin/reports/status)
    or the "Run cron" link in the popular "Admin Menu" module.
9.  All results from your update script will be logged to Drupal's Watchdog for review.
