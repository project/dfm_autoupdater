<?php

/**
 * @file
 * DFM AutoUpdater Configuration.
 */

/**
 * Form builder.
 */
function dfm_autoupdater_settings_form($form, &$form_state) {
  $form['dfm_autoupdater_markup_info'] = array(
    '#type' => 'markup',
    '#markup' => t('Configuration settings to update Drupal Core and Contrib modules on the first cron run every Thursday GMT (after 1700 Wed. Eastern) following Drupal Security Advisory Releases.'),
  );
  $form['dfm_autoupdater_ssh_user'] = array(
    '#type' => 'textfield',
    '#title' => t('AutoUpdate SSH Username'),
    '#description' => t('The ssh username to run the update script under.'),
    '#default_value' => variable_get('dfm_autoupdater_ssh_user', get_current_user()),
    '#required' => TRUE,
  );
  $form['dfm_autoupdater_script_path'] = array(
    '#type' => 'textfield',
    '#size' => 90,
    '#title' => t('AutoUpdate Script Path'),
    '#description' => t('The server path to the update script, including script file name (Drupal private file path is suggested).'),
    '#default_value' => variable_get("dfm_autoupdater_script_path", DRUPAL_ROOT . '/' . variable_get('file_private_path', '') . '/update.sh'),
    '#required' => TRUE,
  );
  $form['dfm_autoupdater_manual_cron_update'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Run updates when cron is invoked manually from Drupal UI (for debugging and/or testing; should normally be turned off).'),
    '#default_value' => variable_get('dfm_autoupdater_manual_cron_update', false),
  );

  return system_settings_form($form);
}

