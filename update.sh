#!/bin/bash

# DFM AutoUpdate script
# Copy this file to your private files directory or wherever configured on the
# DFM AutoUpdater module settings page and then modify it to run whatever update commands
# you need. Make sure it is permissioned to be executable by your SSH user.

# Commands
# Place any commands you need to run here as if you were running the commands yourself 
# while logged in via SSH.  These commands are executed from the Drupal root directory.
# NOTES: 
#   - If using Drush, make sure your commands use the '-y' option to assume 'yes' as answer 
#     to all prompts. This prevents the script from hanging since you can't answer any prompted input.
#   - If using Drush, adding the --nocolor option on commands will also keep your Watchdog's 
#     dfm_autoupdate logs clean and easier to read by stripping terminal color codes from 
#     the script output.
#
# Below is an example update script using drush that updates Core and contrib modules
# then re-applies any previously applied patches using drush-patchfile (https://bitbucket.org/davereid/drush-patchfile).  
# Uncomment the lines below to run them on autoupdate or add your own commands catered for your workflow,
# such as using composer update instead or adding a git commit and push command if desired.
# Basically anything you'd normally do manually when logged in via ssh to update your site.
# drush pm-update --nocolor -y
# drush patch-apply-all --nocolor -y
# drush patch-status --nocolor

